package com.michelKapoko.priceTracker;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PriceTrackerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void demo() throws InterruptedException {
        canCreateANewPriceViaRest();
        canGetAllThePricesForASpecificInstrument();
        canGetAllThePricesForASpecificVendor();
        canUpdateASpecificPriceViaRest();
        pricesOlderThanTwentySecondsAreEvicted(); //20 seconds for test purposes
    }

    private void canCreateANewPriceViaRest() {
        try {
            mvc.perform(
                    post("/price/upsert")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(priceOneCreationRequestJson()))
                    .andExpect(status().isOk())
                    .andExpect(
                            content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(content().string(priceOneCreationResponseJson()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void canGetAllThePricesForASpecificInstrument() {
        try {
            mvc.perform(
                    get("/instrument/1/prices")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(
                            content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(content().string(getInstrumentPricesResponseJson()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void canGetAllThePricesForASpecificVendor() {
        try {
            mvc.perform(
                    get("/vendor/1/prices")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(
                            content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(content().string(getInstrumentPricesResponseJson()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void canUpdateASpecificPriceViaRest() {
        try {
              mvc.perform(
                    post("/price/update")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(priceOneUpdateRequestJson()))
                    .andExpect(status().isOk())
                    .andExpect(
                            content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(content().string(priceOneUpdateResponseJson()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pricesOlderThanTwentySecondsAreEvicted() throws InterruptedException {
           priceUpdatedLessThanTwentySecondsAgoIsReturnedWhenQueriedForAllInstrumentPrices();
           Thread.sleep(30000);
           noPriceIsReturnedWhenQueriedForVendorPrices();
    }

    private void priceUpdatedLessThanTwentySecondsAgoIsReturnedWhenQueriedForAllInstrumentPrices() {
        try {
            mvc.perform(
                    get("/instrument/1/prices")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(
                            content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(content().string(getUpdatedInstrumentPricesResponseJson()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void noPriceIsReturnedWhenQueriedForVendorPrices() {
        try {
            mvc.perform(
                    get("/instrument/1/prices")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(
                            content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
                    )
                    .andExpect(content().string(zeroResultResponseJson()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String priceOneCreationRequestJson() {
        return "{"
            + "\"instrumentId\":1,"
            + "\"vendorId\":1,"
            +  "\"value\":200"
            + "}";
    }

    private String getInstrumentPricesResponseJson() {
        return "[{"
                + "\"priceId\":{\"id\":1},"
                + "\"instrumentId\":{\"id\":1},"
                + "\"vendorId\":{\"id\":1},"
                + "\"value\":200.0"
                + "}]";
    }

    private String zeroResultResponseJson() {
        return "[]";
    }

    private String priceOneUpdateRequestJson() {
        return "{"
                + "\"priceId\":1,"
                +  "\"value\":300"
                + "}";
    }

    private String getUpdatedInstrumentPricesResponseJson() {
        return "[{"
                + "\"priceId\":{\"id\":1},"
                + "\"instrumentId\":{\"id\":1},"
                + "\"vendorId\":{\"id\":1},"
                + "\"value\":300.0"
                + "}]";
    }

    private String priceOneCreationResponseJson() {
        return "{"
                + "\"id\":1"
                + "}";
    }

    private String priceOneUpdateResponseJson() {
        return "true";
    }
}
