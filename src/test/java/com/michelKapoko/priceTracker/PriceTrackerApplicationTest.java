package com.michelKapoko.priceTracker;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
public class PriceTrackerApplicationTest {
    @Test
    public void contextLoads() {
    }
}
