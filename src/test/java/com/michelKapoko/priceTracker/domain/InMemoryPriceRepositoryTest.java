package com.michelKapoko.priceTracker.domain;

import com.michelKapoko.priceTracker.api.InstrumentId;
import com.michelKapoko.priceTracker.api.PriceId;
import com.michelKapoko.priceTracker.api.VendorId;
import com.michelKapoko.priceTracker.exceptions.PriceEntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(MockitoExtension.class)
public class InMemoryPriceRepositoryTest {

    private Clock clock;
    private InMemoryPriceRepository underTest;

    @BeforeEach
    public void setUp(){
        clock = Clock.fixed(Instant.EPOCH, ZoneId.of("UTC"));
        underTest = new InMemoryPriceRepository(clock, 30);
    }

    @Test
    public void throwsPriceEntityNotFoundExceptionWhenPriceEntityDoesNotExist() {
        assertThatExceptionOfType(PriceEntityNotFoundException.class)
                .isThrownBy(() -> { underTest.find(new PriceId(1L));})
                .withMessage("Could not find any PriceEntity with the id PriceId#%d", 1)
                .withNoCause();
    }

    @Test
    public void correctlyGenerateNewIds() {
        PriceId priceId1 = underTest.nextPriceId();
        PriceId priceId2 = underTest.nextPriceId();
        assertThat(priceId1).isEqualTo(new PriceId(1L));
        assertThat(priceId2).isEqualTo(new PriceId(2L));
    }

    @Test
    public void correctlySaveTheNewPriceEntity() {
        PriceId priceId = underTest.nextPriceId();
        PriceEntity priceEntity =
                new PriceEntity(priceId, InstrumentId.of(1L), VendorId.of(1L), 200.0, clock.instant());
        PriceId actual = underTest.save(priceEntity);
        assertThat(actual).isEqualTo(priceId);
        assertThat(underTest.getPricesMap().get(priceId)).isEqualTo(priceEntity);
        assertThat(underTest.getInstrumentToPricesMap().get(InstrumentId.of(1L))).contains(priceEntity.getPriceId());
        assertThat(underTest.getVendorToPricesMap().get(VendorId.of(1L))).contains(priceEntity.getPriceId());
    }

    @Test
    public void correctlySaveTheUpdatedPriceEntity() {
        PriceId priceId = underTest.nextPriceId();
        Clock oneHourEarlierClock = Clock.offset(clock, Duration.ofHours(-1));
        PriceEntity priceEntity = new PriceEntity(priceId, InstrumentId.of(1L), VendorId.of(1L), 200.0,
                oneHourEarlierClock.instant());
        underTest.save(priceEntity);
        PriceEntity newPriceEntity =
                new PriceEntity(priceId, InstrumentId.of(1L), VendorId.of(1L), 300.0, clock.instant());
        PriceId actual= underTest.save(newPriceEntity);

        assertThat(actual).isEqualTo(priceId);
        assertThat(underTest.getPricesMap().get(priceId)).isEqualTo(newPriceEntity);
        assertThat(underTest.getInstrumentToPricesMap().get(InstrumentId.of(1L))).containsExactly(priceId);
        assertThat(underTest.getVendorToPricesMap().get(VendorId.of(1L))).containsExactly(priceId);
    }

    @Test
    public void correctlySearchBasedOnInstrumentdAndVendorId() {
        PriceId priceId1 = underTest.nextPriceId();
        PriceEntity priceEntity1 =
                new PriceEntity(priceId1, InstrumentId.of(1L), VendorId.of(1L), 200.0, clock.instant());
        underTest.save(priceEntity1);

        PriceId priceId2 = underTest.nextPriceId();
        PriceEntity priceEntity2 =
                new PriceEntity(priceId2, InstrumentId.of(2L), VendorId.of(1L), 200.0, clock.instant());
        underTest.save(priceEntity2);

        PriceId priceId3 = underTest.nextPriceId();
        PriceEntity priceEntity3 =
                new PriceEntity(priceId3, InstrumentId.of(2L), VendorId.of(2L), 200.0, clock.instant());
        underTest.save(priceEntity3);

        Optional<PriceEntity> actual = underTest.find(InstrumentId.of(1L), VendorId.of(1L));

        assertThat(actual).isEqualTo(Optional.of(priceEntity1));
    }

    @Test
    public void returnAnEmptyListWhenQueriedForAllThePricesOfASingleInstrumentAndTheInstrumentIsNotInTheCache() {
        assertThat(underTest.getPrices(InstrumentId.of(1L)).isEmpty());
    }

    @Test
    public void returnTheCorrectListOfPricesForAnInstrument() {
        PriceId priceId1 = underTest.nextPriceId();
        PriceEntity priceEntity1 =
                new PriceEntity(priceId1, InstrumentId.of(1L), VendorId.of(11L), 300.0, clock.instant());
        underTest.save(priceEntity1);

        PriceId priceId2 = underTest.nextPriceId();
        PriceEntity priceEntity2 =
                new PriceEntity(priceId2, InstrumentId.of(1L), VendorId.of(11L), 300.0, clock.instant());
        underTest.save(priceEntity2);

        PriceId priceId3 = underTest.nextPriceId();
        PriceEntity priceEntity3 =
                new PriceEntity(priceId3, InstrumentId.of(2L), VendorId.of(11L), 300.0, clock.instant());
        underTest.save(priceEntity3);

        assertThat(underTest.getPrices(InstrumentId.of(1L))).containsExactlyInAnyOrder(priceEntity1,priceEntity2);
        assertThat(underTest.getPrices(InstrumentId.of(2L))).containsExactly(priceEntity3);
    }

    @Test
    public void returnAnEmptyListIfThereAreNoPricesForASpecificVendorInTheCache() {
        assertThat(underTest.getPrices(VendorId.of(1L)).isEmpty());
    }

    @Test
    public void returnTheCorrectListOfPricesForAVendor() {
        PriceId priceId1 = underTest.nextPriceId();
        PriceEntity priceEntity1 =
                new PriceEntity(priceId1, InstrumentId.of(1L), VendorId.of(11L), 300.0, clock.instant());
        underTest.save(priceEntity1);

        PriceId priceId2 = underTest.nextPriceId();
        PriceEntity priceEntity2 =
                new PriceEntity(priceId2, InstrumentId.of(1L), VendorId.of(12L), 300.0, clock.instant());
        underTest.save(priceEntity2);

        PriceId priceId3 = underTest.nextPriceId();
        PriceEntity priceEntity3 =
                new PriceEntity(priceId3, InstrumentId.of(2L), VendorId.of(12L), 300.0, clock.instant());
        underTest.save(priceEntity3);

        assertThat(underTest.getPrices(VendorId.of(11L))).containsExactlyInAnyOrder(priceEntity1);
        assertThat(underTest.getPrices(VendorId.of(12L))).containsExactly(priceEntity2, priceEntity3);
    }

    @Test
    public void repositoryPurgeWorks() {
        PriceId priceId1 = underTest.nextPriceId();
        PriceEntity priceEntity1 =
                new PriceEntity(priceId1, InstrumentId.of(1L), VendorId.of(11L), 300.0, clock.instant());
        underTest.save(priceEntity1);

        Clock oneHourEarlierClock = Clock.offset(clock, Duration.ofHours(-1));
        PriceId priceId2 = underTest.nextPriceId();
        PriceEntity priceEntity2 =
                new PriceEntity(priceId2, InstrumentId.of(1L), VendorId.of(12L), 300.0, oneHourEarlierClock.instant());
        underTest.save(priceEntity2);

        PriceId priceId3 = underTest.nextPriceId();
        PriceEntity priceEntity3 =
                new PriceEntity(priceId3, InstrumentId.of(2L), VendorId.of(12L), 300.0, oneHourEarlierClock.instant());
        underTest.save(priceEntity3);

        ConcurrentMap<PriceId, PriceEntity> expectedPriceMap = new ConcurrentHashMap<>();
        ConcurrentMap<InstrumentId, Set<PriceId>> expectedInstrumentPriceMap = new ConcurrentHashMap<>();
        ConcurrentMap<VendorId, Set<PriceId>> expectedVendorPriceMap = new ConcurrentHashMap<>();
        expectedPriceMap.put(priceId1, priceEntity1);
        expectedInstrumentPriceMap.put(InstrumentId.of(1L), new HashSet<PriceId>(Arrays.asList(priceId1)));
        expectedVendorPriceMap.put(VendorId.of(11L), new HashSet<PriceId>(Arrays.asList(priceId1)));

        underTest.purge();
        assertThat(underTest.getPricesMap()).isEqualTo(expectedPriceMap);
        assertThat(underTest.getInstrumentToPricesMap()).isEqualTo(expectedInstrumentPriceMap);
        assertThat(underTest.getVendorToPricesMap()).isEqualTo(expectedVendorPriceMap);
    }
}

