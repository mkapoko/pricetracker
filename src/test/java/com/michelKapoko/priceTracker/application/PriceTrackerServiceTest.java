package com.michelKapoko.priceTracker.application;

import com.michelKapoko.priceTracker.api.*;
import com.michelKapoko.priceTracker.domain.PriceEntity;
import com.michelKapoko.priceTracker.domain.PriceRepository;
import com.michelKapoko.priceTracker.exceptions.PriceEntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PriceTrackerServiceTest {

    @Mock
    private PriceRepository priceRepository;
    private Clock clock;
    private ArgumentCaptor<PriceEntity> priceCaptor;

    private PriceTracker underTest;


    @BeforeEach
    public void setUp(){
        clock = Clock.fixed(Instant.EPOCH, ZoneId.systemDefault());
        underTest = new PriceTrackerService(priceRepository, clock);
        priceCaptor =  ArgumentCaptor.forClass(PriceEntity.class);
    }

    @Test
    public void invokesTheRepositoryAppropriatelyWhenUpsertingANewPrice() {
        UpsertPriceCommand command = UpsertPriceCommand.of(InstrumentId.of(1L), VendorId.of(1L), 200.0);
        when(priceRepository.find(any(), any())).thenReturn(Optional.empty());
        when(priceRepository.nextPriceId()).thenReturn(PriceId.of(1L));

        PriceEntity expectedArgument =
                new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0, clock.instant());

        underTest.upsertPrice(command);
        verify(priceRepository).save(priceCaptor.capture());
        assertThat(priceCaptor.getValue()).isEqualTo(expectedArgument);
    }

    @Test
    public void invokesTheRepositoryAppropriatelyWhenUpsertingAnExistingPrice() {
        Clock oneHourEarlierClock = Clock.offset(clock, Duration.ofHours(-1));
        PriceEntity existingPrice =
                new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0, oneHourEarlierClock.instant());

        when(priceRepository.find(any(), any())).thenReturn(Optional.of(existingPrice));
        UpsertPriceCommand command = UpsertPriceCommand.of(InstrumentId.of(1L), VendorId.of(1L), 300.0);

        PriceEntity expectedArgument =
                new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 300.0, clock.instant());

        underTest.upsertPrice(command);
        verify(priceRepository).save(priceCaptor.capture());
        assertThat(priceCaptor.getValue()).isEqualTo(expectedArgument);
    }

    @Test
    public void invokesTheRepositoryAppropriatelyWhenUpdatingAPrice() {
        Clock oneHourEarlierClock = Clock.offset(clock, Duration.ofHours(-1));
        PriceEntity initialPrice =
                new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0,
                        oneHourEarlierClock.instant());
        when(priceRepository.find(eq(PriceId.of(1L)))).thenReturn(initialPrice);

        UpdatePriceCommand command = UpdatePriceCommand.of(PriceId.of(1L), 300.0);

        PriceEntity expectedArgument =
                new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 300.0, clock.instant());

        boolean actualOutput = underTest.updatePrice(command);
        verify(priceRepository).save(priceCaptor.capture());
        assertThat(priceCaptor.getValue()).isEqualTo(expectedArgument);
        assertThat(actualOutput).isTrue();
    }

    @Test
    public void returnsTrueWhenPriceIsUpdated() {
        Clock oneHourEarlierClock = Clock.offset(clock, Duration.ofHours(-1));
        PriceEntity initialPrice =
                new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0,
                        oneHourEarlierClock.instant());
        when(priceRepository.find(eq(PriceId.of(1L)))).thenReturn(initialPrice);

        UpdatePriceCommand command = UpdatePriceCommand.of(PriceId.of(1L), 300.0);

        boolean actual = underTest.updatePrice(command);
        assertThat(actual).isTrue();
    }

    @Test
    public void returnsFalseWhenPriceIsNotUpdated() {
        Clock oneHourEarlierClock = Clock.offset(clock, Duration.ofHours(-1));
        PriceEntity initialPrice =
                new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0,
                        oneHourEarlierClock.instant());
        when(priceRepository.find(eq(PriceId.of(1L)))).thenReturn(initialPrice);

        UpdatePriceCommand command = UpdatePriceCommand.of(PriceId.of(1L), 200.0);

        boolean actual = underTest.updatePrice(command);
        assertThat(actual).isFalse();
    }

    @Test
    public void throwsPriceEntityNotFoundExceptionWhenPriceToUpdateDoesNotExist() {
        UpdatePriceCommand command = UpdatePriceCommand.of(PriceId.of(1L), 300.0);
        when(priceRepository.find(eq(PriceId.of(1L))))
                .thenThrow(new PriceEntityNotFoundException("Could not find any PriceEntity with the id PriceId#1"));

        assertThatExceptionOfType(PriceEntityNotFoundException.class)
                .isThrownBy(() -> { underTest.updatePrice(command);})
                .withMessage("Could not find any PriceEntity with the id PriceId#1")
                .withNoCause();
    }

    @Test
    public void returnAnEmptyListWhenQueriedForAllThePricesOfASingleInstrumentAndTheInstrumentIsNotInTheCache() {
        when(priceRepository.getPrices(eq(InstrumentId.of(1L)))).thenReturn(Collections.emptyList());
        assertThat(underTest.getInstrumentPrices(InstrumentId.of(1L)).isEmpty());
    }

    @Test
    public void returnListOfPricesWhenQueriedForAllThePricesOfASingleInstrumentAndInstrumentIsInTheCache() {
        Price vendorOnePrice = new Price(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0);
        Price vendorTwoPrice = new Price(PriceId.of(2L), InstrumentId.of(1L), VendorId.of(2L), 300.0);

        PriceEntity vendorOnePriceEntity = new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0, clock.instant());
        PriceEntity vendorTwoPriceEntity = new PriceEntity(PriceId.of(2L), InstrumentId.of(1L), VendorId.of(2L), 300.0, clock.instant());

        when(priceRepository.getPrices(eq(InstrumentId.of(1L))))
                .thenReturn(Arrays.asList(vendorOnePriceEntity, vendorTwoPriceEntity));

        assertThat(underTest.getInstrumentPrices(InstrumentId.of(1L))).containsExactlyInAnyOrder(vendorOnePrice, vendorTwoPrice);
    }

    @Test
    public void returnAnEmptyListIfThereAreNoPricesForASpecificVendorInTheCache() {
        when(priceRepository.getPrices(eq(VendorId.of(1L)))).thenReturn(Collections.emptyList());
        assertThat(underTest.getVendorPrices(VendorId.of(1L)).isEmpty());
    }

    @Test
    public void returnListOfPricesWhenQueriedForAllThePricesAssociatedToAVendorInTheCache() {
        Price instOnePrice = new Price(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0);
        Price instTwoPrice = new Price(PriceId.of(2L), InstrumentId.of(2L), VendorId.of(1L), 300.0);

        PriceEntity instOnePriceEntity = new PriceEntity(PriceId.of(1L), InstrumentId.of(1L), VendorId.of(1L), 200.0, clock.instant());
        PriceEntity instTwoPriceEntity = new PriceEntity(PriceId.of(2L), InstrumentId.of(2L), VendorId.of(1L), 300.0, clock.instant());

        when(priceRepository.getPrices(eq(VendorId.of(1L))))
                .thenReturn(Arrays.asList(instOnePriceEntity, instTwoPriceEntity));

        assertThat(underTest.getVendorPrices(VendorId.of(1L))).containsExactlyInAnyOrder(instOnePrice, instTwoPrice);
    }
}
