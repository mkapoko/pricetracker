package com.michelKapoko.priceTracker.application;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class PriceRepositoryPurgeSchedulerTest {

    @SpyBean
    private PriceRepositoryPurgeScheduler priceRepositoryPurgeScheduler;

    @Test
    public void whenWaitFiveSecondsThenPurgeCalledAtLeastTwoTimes() {
        await()
                .atMost(5, TimeUnit.SECONDS)
                .untilAsserted(() -> verify(priceRepositoryPurgeScheduler, atLeast(2)).purge());
    }
}
