package com.michelKapoko.priceTracker.adapter;

import com.michelKapoko.priceTracker.api.*;
import com.michelKapoko.priceTracker.application.PriceTrackerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class PriceTrackerController implements PriceTracker {

    private final PriceTrackerService priceTrackerService;

    public PriceTrackerController(PriceTrackerService priceTrackerService) {
        this.priceTrackerService = priceTrackerService;
    }

    @Override
    @PostMapping(value = "/price/upsert", consumes = APPLICATION_JSON_VALUE)
    public PriceId upsertPrice(@RequestBody UpsertPriceCommand command) {
        return priceTrackerService.upsertPrice(command);
    }

    @Override
    @PostMapping(value = "/price/update", consumes = APPLICATION_JSON_VALUE)
    public boolean updatePrice(@RequestBody UpdatePriceCommand command) {
        return priceTrackerService.updatePrice(command);
    }

    @Override
    @GetMapping(value = "/instrument/{instrumentId}/prices", produces = APPLICATION_JSON_VALUE)
    public List<Price> getInstrumentPrices(@PathVariable InstrumentId instrumentId) {
        return priceTrackerService.getInstrumentPrices(instrumentId);
    }

    @Override
    @GetMapping(value = "/vendor/{vendorId}/prices", produces = APPLICATION_JSON_VALUE)
    public List<Price> getVendorPrices(@PathVariable VendorId vendorId) {
        return priceTrackerService.getVendorPrices(vendorId);
    }
}
