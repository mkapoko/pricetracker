package com.michelKapoko.priceTracker.application;

import com.michelKapoko.priceTracker.api.*;
import com.michelKapoko.priceTracker.domain.PriceEntity;
import com.michelKapoko.priceTracker.domain.PriceRepository;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PriceTrackerService implements PriceTracker {

    private final PriceRepository priceRepository;
    private final Clock clock;

    public PriceTrackerService(PriceRepository priceRepository, Clock clock) {
        this.priceRepository = priceRepository;
        this.clock = clock;
    }

    @Override
    public PriceId upsertPrice(UpsertPriceCommand command) {
        Optional<PriceEntity> priceEntityOptional = priceRepository.find(command.getInstrumentId(), command.getVendorId());
        PriceEntity priceEntity;
        if(priceEntityOptional.isEmpty()){
            priceEntity = new PriceEntity(
                    priceRepository.nextPriceId(),
                    command.getInstrumentId(),
                    command.getVendorId(),
                    command.getValue(),
                    clock.instant());
        } else {
          priceEntity = priceEntityOptional.get();
          priceEntity.setValue(command.getValue());
          priceEntity.setLastUpdated(clock.instant());
        }
        return priceRepository.save(priceEntity);
    }

    @Override
    public boolean updatePrice(UpdatePriceCommand command) {
        PriceEntity priceEntity = priceRepository.find(command.getPriceId());
        if (priceEntity.getValue() != command.getValue()) {
            priceEntity.setValue(command.getValue());
            priceEntity.setLastUpdated(clock.instant());
            priceRepository.save(priceEntity);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Price> getInstrumentPrices(InstrumentId instrumentId) {
        return mapPrices(priceRepository.getPrices(instrumentId));
    }

    @Override
    public List<Price> getVendorPrices(VendorId vendorId) {
        return mapPrices(priceRepository.getPrices(vendorId));
    }

    private List<Price> mapPrices(List<PriceEntity> prices) {
        List<Price> priceList = prices.stream()
                .map(this::mapPrice)
                .collect(Collectors.toList());
        return priceList;
    }

    private Price mapPrice(PriceEntity priceEntity) {
        return new Price(
                priceEntity.getPriceId(),
                priceEntity.getInstrumentId(),
                priceEntity.getVendorId(),
                priceEntity.getValue());
    }
}
