package com.michelKapoko.priceTracker.application;

import com.michelKapoko.priceTracker.domain.PriceRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Component used to make sure that stale prices are evicted from the repository on a regular basis.
 */
@Component
public class PriceRepositoryPurgeScheduler {

    private PriceRepository priceRepository;

    public PriceRepositoryPurgeScheduler(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    /**
     * We purge the repository every day at 01:00 am which normally should be a time at which this purge
     * would have the least impact.
     * Purging daily seems a reasonable frequency in regards to the time to live of an item in the cache which is 30 days
     */
    @Scheduled(cron="${price-repository-purge-cron-expression:0 0 1 * * ?}")
    public void purge() {
        priceRepository.purge();
    }
}
