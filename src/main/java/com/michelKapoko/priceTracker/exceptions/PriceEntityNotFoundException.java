package com.michelKapoko.priceTracker.exceptions;

import com.michelKapoko.priceTracker.api.PriceId;

/**
 * Exception thrown when no PriceEntity is found which matches a specific PriceId.
 * @see com.michelKapoko.priceTracker.domain.InMemoryPriceRepository#find(PriceId) 
 */
public class PriceEntityNotFoundException extends RuntimeException {

    public PriceEntityNotFoundException (String message) {
        super(message);
    }

}
