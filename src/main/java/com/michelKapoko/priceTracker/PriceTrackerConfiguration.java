package com.michelKapoko.priceTracker;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.Clock;

@Configuration
@EnableScheduling
@ComponentScan("com.michelkapoko.priceTracker")
public class PriceTrackerConfiguration {
    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }
}