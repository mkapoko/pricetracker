package com.michelKapoko.priceTracker.api;

import java.util.Objects;

/**
 * Value Object which represents the Id of an instrument.
 * This value is created in another service and pass through to this application when creating a new price.
 */
public class InstrumentId {
    private final Long id;

    public InstrumentId(Long id) {
        this.id = id;
    }

    public InstrumentId(String id) {
        this.id = Long.valueOf(id);
    }

    public static InstrumentId of(Long id) {
        return new InstrumentId(id);
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InstrumentId that = (InstrumentId) o;
        return Objects.equals(id, that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "InstrumentId{" +
                "id=" + id +
                '}';
    }
}
