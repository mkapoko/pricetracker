package com.michelKapoko.priceTracker.api;

/**
 * Value object containing the information necessary for the update of the value of a price.
 * All the clients desiring to update the value of a specific price for which it has an ID
 * will request it with an instance of this command.
 */
public class UpdatePriceCommand {
    private final PriceId priceId;
    private final double value;

    public UpdatePriceCommand(PriceId priceId, double value) {
        this.priceId = priceId;
        this.value = value;
    }

    public static UpdatePriceCommand of(PriceId priceId, double value) {
        return new UpdatePriceCommand(priceId, value);
    }

    public PriceId getPriceId() {
        return priceId;
    }

    public double getValue() {
        return value;
    }
}
