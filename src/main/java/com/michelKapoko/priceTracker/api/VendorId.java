package com.michelKapoko.priceTracker.api;

import java.util.Objects;

/**
 * Value Object which represents the Id of a vendor.
 * This value is created in another service and pass through to this application when creating a new price.
 */
public class VendorId {
    private final Long id;

    public VendorId(Long id) {
        this.id = id;
    }

    public VendorId(String id) {
        this.id = Long.valueOf(id);
    }

    public static VendorId of(Long id) {
        return new VendorId(id);
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VendorId that = (VendorId) o;
        return Objects.equals(id, that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "VendorId{" +
                "id=" + id +
                '}';
    }
}
