package com.michelKapoko.priceTracker.api;

import java.util.List;

/**
 * Interface defining all the services provided by our application to external clients
 */
public interface PriceTracker {

    /**
     * Insert a new price in the repository if none already exists for the combination InstrumentId + VendorId.
     * Otherwise update the one which already exists.
     * This will typically be used by clients which have no way to figure out whether a price already exists or not
     * but to make a query to this application (e.g: A consumer of an external market price event which generates internal price information based
     * on the event).
     *
     * The additional request is avoided by giving the flexibility to the client to update a specific price if it already existed.
     * @param command a command of type {@link UpsertPriceCommand})
     * @return PriceId  the id of the created or updated price.
     */
    PriceId upsertPrice(UpsertPriceCommand command);

    /**
     * Update the value of the price.
     * @param command a command of type {@link UpdatePriceCommand})
     * @return true/false true if the Price was indeed updated, false otherwise
     */
    boolean updatePrice(UpdatePriceCommand command);

    /**
     * @param instrumentId The id of the instrument for which we would like to retrieve prices)
     * @return The list of all the prices which are associated to this instrument.
     */
    List<Price> getInstrumentPrices(InstrumentId instrumentId);

    /**
     * @param vendorId The id of the vendor for which we would like to retrieve prices)
     * @return The list of all the prices which are associated to this vendor in the repository.
     */
    List<Price> getVendorPrices(VendorId vendorId);
}
