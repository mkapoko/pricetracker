package com.michelKapoko.priceTracker.api;

import java.util.Objects;

/**
 * Value Object which is the DTO equivalent of the PriceEntity.
 * It is under packaged as an instance of this object that all the requested prices leave the boundaries of the application.
 */
public class Price {
    private final PriceId priceId;
    private final InstrumentId instrumentId;
    private final VendorId vendorId;
    private final double value;

    public Price(PriceId priceId, InstrumentId instrumentId, VendorId vendorId, double value) {
        this.priceId = priceId;
        this.instrumentId = instrumentId;
        this.vendorId = vendorId;
        this.value = value;
    }

    public InstrumentId getInstrumentId() {
        return instrumentId;
    }

    public VendorId getVendorId() {
        return vendorId;
    }

    public double getValue() {
        return value;
    }

    public PriceId getPriceId() {
        return priceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Double.compare(price.value, value) == 0 &&
                Objects.equals(priceId, price.priceId) &&
                Objects.equals(instrumentId, price.instrumentId) &&
                Objects.equals(vendorId, price.vendorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priceId, instrumentId, vendorId, value);
    }
}
