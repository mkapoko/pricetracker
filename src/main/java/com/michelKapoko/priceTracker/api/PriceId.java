package com.michelKapoko.priceTracker.api;

import java.util.Objects;

/**
 * Value Object which represents the Id of a price.
 */
public class PriceId {
    private final Long id;

    public PriceId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public static PriceId of(Long id) {
        return new PriceId(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceId that = (PriceId) o;
        return Objects.equals(id, that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "PriceId{" +
                "id=" + id +
                '}';
    }
}
