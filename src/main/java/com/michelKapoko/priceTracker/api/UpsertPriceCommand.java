package com.michelKapoko.priceTracker.api;

/**
 * Value object containing the information necessary for the creation of a  new price.
 * All the clients desiring to create a new price (or potentially update the value of a
 * price for which they don't have an ID @see PriceTracker#upsertPrice)
 * will request it with an instance of this command.
 */
public class UpsertPriceCommand {
    private final InstrumentId instrumentId;
    private final VendorId vendorId;
    private final double value;

    public UpsertPriceCommand(InstrumentId instrumentId, VendorId vendorId, double value) {
        this.instrumentId = instrumentId;
        this.vendorId = vendorId;
        this.value = value;
    }

    public static UpsertPriceCommand of(InstrumentId instrumentId, VendorId vendorId, double value) {
        return new UpsertPriceCommand(instrumentId, vendorId, value);
    }

    public InstrumentId getInstrumentId() {
        return instrumentId;
    }

    public VendorId getVendorId() {
        return vendorId;
    }

    public Double getValue() {
        return value;
    }
}
