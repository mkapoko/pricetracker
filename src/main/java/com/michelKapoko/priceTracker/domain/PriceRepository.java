package com.michelKapoko.priceTracker.domain;

import com.michelKapoko.priceTracker.api.InstrumentId;
import com.michelKapoko.priceTracker.api.PriceId;
import com.michelKapoko.priceTracker.api.VendorId;

import java.util.List;
import java.util.Optional;

/**
 * Interface defining all the services provided by a repository to its clients
 */
public interface PriceRepository {

    /**
     * @return A priceId which has not yet been associated to any price
     */
    PriceId nextPriceId();

    /**
     * Save an entity in the repository.
     * @param priceEntity the entity to register
     * @return PriceId The id of the entity which has just been saved in the repository
     */
    PriceId save(PriceEntity priceEntity);

    /**
     * Find a specific PriceEntity by its ID
     * @param priceId
     * @return The entity whose ID is provided
     */
    PriceEntity find(PriceId priceId);

    /**
     * Find a specific PriceEntity by its instrumentId and vendorId
     * @param instrumentId,vendorId
     * @return The price entity associated to the provided combination of instrumentId and vendorId
     */
    Optional<PriceEntity> find(InstrumentId instrumentId, VendorId vendorId);

    /**
     * @param instrumentId The id of the instrument for which we would like to retrieve prices)
     * @return The list of all the PriceEntity which are associated to this instrument.
     */
    List<PriceEntity> getPrices(InstrumentId instrumentId);

    /**
     * @param vendorId The id of the vendor for which we would like to retrieve prices)
     * @return The list of all the PriceEntity which are associated to this vendor.
     */
    List<PriceEntity> getPrices(VendorId vendorId);

    /**
     * Evict stale items from the repository.
     */
    void purge();
}
