package com.michelKapoko.priceTracker.domain;

import com.michelKapoko.priceTracker.api.InstrumentId;
import com.michelKapoko.priceTracker.api.PriceId;
import com.michelKapoko.priceTracker.api.VendorId;

import java.time.Instant;
import java.util.Objects;

public class PriceEntity {
    private PriceId priceId;
    private InstrumentId instrumentId;
    private VendorId vendorId;
    private Double value;
    private Instant lastUpdated;

    public PriceEntity(
            PriceId priceId,
            InstrumentId instrumentId,
            VendorId vendorId,
            Double value,
            Instant lastUpdated) {
        this.priceId = priceId;
        this.instrumentId = instrumentId;
        this.vendorId = vendorId;
        this.value = value;
        this.lastUpdated = lastUpdated;
    }

    public InstrumentId getInstrumentId() {
        return instrumentId;
    }

    public VendorId getVendorId() {
        return vendorId;
    }

    public Double getValue() {
        return value;
    }

    public PriceId getPriceId() {
        return priceId;
    }

    public void setValue(double value) {
        this.value =  value;
    }

    public Instant getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Instant instant) {
        this.lastUpdated = instant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceEntity that = (PriceEntity) o;
        return Objects.equals(priceId, that.priceId) &&
                Objects.equals(instrumentId, that.instrumentId) &&
                Objects.equals(vendorId, that.vendorId) &&
                Objects.equals(value, that.value) &&
                Objects.equals(lastUpdated, that.lastUpdated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priceId, instrumentId, vendorId, value, lastUpdated);
    }

    @Override
    public String toString() {
        return "PriceEntity{" +
                "priceId=" + priceId +
                ", instrumentId=" + instrumentId +
                ", vendorId=" + vendorId +
                ", value=" + value +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
