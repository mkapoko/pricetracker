package com.michelKapoko.priceTracker.domain;

import com.michelKapoko.priceTracker.api.InstrumentId;
import com.michelKapoko.priceTracker.api.PriceId;
import com.michelKapoko.priceTracker.api.VendorId;
import com.michelKapoko.priceTracker.exceptions.PriceEntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class InMemoryPriceRepository implements PriceRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(InMemoryPriceRepository.class);
    private final ConcurrentMap<PriceId, PriceEntity> prices;
    private final ConcurrentMap<InstrumentId, Set<PriceId>> instrumentToPrices;
    private final ConcurrentMap<VendorId, Set<PriceId>> vendorToPrices;
    private final Clock clock;
    private AtomicInteger lastUsedId;
    private long timeToLiveInSeconds;

    public InMemoryPriceRepository(Clock clock, @Value("${price-time-to-live-in-seconds:2592000}") long timeToLiveInSeconds) {
        this.clock = clock;
        this.timeToLiveInSeconds = timeToLiveInSeconds;
        this.lastUsedId = new AtomicInteger();
        this.prices = new ConcurrentHashMap<>();
        this.instrumentToPrices = new ConcurrentHashMap<>();
        this.vendorToPrices = new ConcurrentHashMap<>();
    }

    @Override
    public PriceId nextPriceId() {
        return new PriceId(Long.valueOf(lastUsedId.addAndGet(1)));
    }

    @Override
    public PriceEntity find(PriceId priceId) {
        PriceEntity price = prices.get(priceId);
        if(price != null) {
            return price;
        }
        throw (new PriceEntityNotFoundException("Could not find any PriceEntity with the id PriceId#" + priceId.getId()));
    }

    @Override
    public Optional<PriceEntity> find(InstrumentId instrumentId, VendorId vendorId) {
        return prices.entrySet()
                .stream()
                .filter(entry -> entry.getValue().getInstrumentId().equals(instrumentId) && entry.getValue().getVendorId().equals(vendorId))
                //if there is any such price, there should be only one instance in the map
                .findFirst()
                .map(entry -> entry.getValue());
    }

    @Override
    public PriceId save(PriceEntity priceEntity) {
        prices.put(priceEntity.getPriceId(), priceEntity);
        instrumentToPrices.computeIfAbsent(priceEntity.getInstrumentId(), k-> new HashSet<>()).add(priceEntity.getPriceId());
        vendorToPrices.computeIfAbsent(priceEntity.getVendorId(), k-> new HashSet<>()).add(priceEntity.getPriceId());
        return priceEntity.getPriceId();
    }

    @Override
    public List<PriceEntity> getPrices(InstrumentId instrumentId) {
        Optional<Set<PriceId>> priceIds = Optional.ofNullable(instrumentToPrices.get(instrumentId));

        return priceIds.orElseGet(() -> Collections.emptySet()).stream()
                .map(priceId -> prices.get(priceId))
                .collect(Collectors.toList());
    }

    @Override
    public List<PriceEntity> getPrices(VendorId vendorId) {
        Optional<Set<PriceId>> priceIds = Optional.ofNullable(vendorToPrices.get(vendorId));

        return priceIds.orElseGet(() -> Collections.emptySet()).stream()
                .map(priceId -> prices.get(priceId))
                .collect(Collectors.toList());
    }

    @Override
    public void purge() {
        LOGGER.info("Starting the eviction of old prices from the repository");
        List<PriceEntity> evictedPrices = new ArrayList<>();
        for(Map.Entry<PriceId, PriceEntity> entry : prices.entrySet() ) {
            if(isStale(entry.getValue())) {
                prices.remove(entry.getKey());

                instrumentToPrices.get(entry.getValue().getInstrumentId()).remove(entry.getKey());
                if(instrumentToPrices.get(entry.getValue().getInstrumentId()).isEmpty()) {
                    instrumentToPrices.remove(entry.getValue().getInstrumentId());
                }

                vendorToPrices.get(entry.getValue().getVendorId()).remove(entry.getKey());
                if(vendorToPrices.get(entry.getValue().getVendorId()).isEmpty()) {
                    vendorToPrices.remove(entry.getValue().getVendorId());
                }
                evictedPrices.add(entry.getValue());
            }
        }
        LOGGER.info("Done Evicting old prices. The prices evicted are: {}", evictedPrices.toString());
    }

    private boolean isStale(PriceEntity priceEntity) {
        return (clock.instant().getEpochSecond() - priceEntity.getLastUpdated().getEpochSecond()) > timeToLiveInSeconds;
    }

    public ConcurrentMap<PriceId, PriceEntity> getPricesMap() {
        return prices;
    }

    public ConcurrentMap<InstrumentId, Set<PriceId>> getInstrumentToPricesMap() {
        return instrumentToPrices;
    }

    public ConcurrentMap<VendorId, Set<PriceId>> getVendorToPricesMap() {
        return vendorToPrices;
    }
}
