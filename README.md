# General note:
Design details and explanation are provided in the document `TakeHomeProjectReport_MichelKapoko.pdf`

#How to start the application
This application can be started as any spring boot application, ie:
1. Make sure you are in the root of the directory of the associate repository
2. run `mvn spring-boot:run`

Once the application has started up, the different services provided are reachable at the endpoint: 
`http://localhost:8081/`

#Sample curl requests :
### Create a new price 
```
curl \
    -v \
    -X POST http://localhost:8081/price/upsert \
    --header "Content-Type: application/json" \
    --data "{
            \"instrumentId\":1,
            \"vendorId\":1,
            \"value\":200
    }"
```

### Get all the prices associated to instrumentId 1:
 `curl http://localhost:8081/instrument/1/prices`


### Get all the prices associated to vendorId 1:
 `curl http://localhost:8081/vendor/1/prices`

### Update a price 
```
curl \
    -v \
    -X POST http://localhost:8081/price/update \
    --header "Content-Type: application/json" \
    --data "{
            \"priceId\":1,
            \"value\":300
    }"
```
